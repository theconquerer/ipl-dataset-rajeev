const fs = require('fs');
const csvToJson = require('convert-csv-to-json');
const {
  matchesWonPerTeamPerYear,
  matchesPlayedPerYear,
  extraRunsConcededPerTeamIn2016,
  top10EconomicalBowlerIn2015
} = require('./ipl');

/**
 * Fetches CSV File, converts it and returns its Object.
 *
 * @param {string} csvFilePath Path of CSV File.
 * @returns {Object} Object literal of CSV File.
 */
function getJson(csvFilePath) {
  return csvToJson.fieldDelimiter(',').getJsonFromCsv(csvFilePath);
}
/**
 * Writes the provided object literal in a JSON file
 *
 * @param {sting} name Filename to save with.
 * @param {Object} obj Object to written
 */
function writeMyJSONFile(name, obj) {
  const stringifiedobject = JSON.stringify(obj);
  fs.writeFileSync(`../output/${name}.json`, stringifiedobject);
}

const matches = getJson('../data/matches.csv');
const deliveries = getJson('../data/deliveries.csv');

writeMyJSONFile(`matchesPlayedPerYear`, matchesPlayedPerYear(matches));

writeMyJSONFile(`matchesWonPerTeamPerYear`, matchesWonPerTeamPerYear(matches));

writeMyJSONFile(
  `extraRunsConcededPerTeamIn2016`,
  extraRunsConcededPerTeamIn2016(matches, deliveries)
);

writeMyJSONFile(
  `top10EconomicalBowlerIn2015`,
  top10EconomicalBowlerIn2015(matches, deliveries)
);
