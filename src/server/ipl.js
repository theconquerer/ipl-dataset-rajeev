/**
 * Calculates total matches played in every season of IPL.
 *
 * @param {Array} matches [{ match },...]
 * @returns {Object} matchesPlayedObj { season: matches played in season}
 * @exports
 */
function matchesPlayedPerYear(matches) {
  const matchesPlayedObj = matches.reduce((matchesPlayedObj, match) => {
    matchesPlayedObj[match.season] = (matchesPlayedObj[match.season] || 0) + 1;
    return matchesPlayedObj;
  }, {});
  return matchesPlayedObj;
}

/**
 * Calculates matches won by every team in every season of IPL.
 *
 * @param {Array} matches [{ match },...]
 * @returns {Object} matchesWonObj { season: { team: matches won by team in season}}
 * @exports
 */
function matchesWonPerTeamPerYear(matches) {
  const matchesWonObj = matches.reduce((matchesWonObj, match) => {
    if (!matchesWonObj.hasOwnProperty(match.season)) {
      matchesWonObj[match.season] = {};
    }
    matchesWonObj[match.season][match.winner] =
      (matchesWonObj[match.season][match.winner] || 0) + 1;
    return matchesWonObj;
  }, {});
  //where no one won, it was an empty string. To clean that out...
  Object.keys(matchesWonObj).map(season => {
    if (matchesWonObj[season].hasOwnProperty('')) {
      delete matchesWonObj[season][''];
    }
    return 0;
  });
  return matchesWonObj;
}

/**
 * Filters out delivery data using matches data and provided season
 *
 * @param {string} season 'season'
 * @param {Array} matches [{ match },...]
 * @param {Array} deliveries [{ delivery },...]
 * @returns {Array} deliveries [{ delivery },...]
 */
function getDataForYear(season, matches, deliveries) {
  const idsForMatches = matches.map(match => {
    if (match.season == season) {
      return match.id;
    }
  });
  const lowestMatchID = Math.min(...idsForMatches);
  const highestMatchID = Math.max(...idsForMatches);
  return (
    deliveries
      //filter out the data between extreme IDs
      .filter(
        delivery =>
          +delivery.match_id >= lowestMatchID &&
          +delivery.match_id <= highestMatchID
      )
  );
}

/**
 * Calculates runs conceded by every team in IPL season 2016.
 *
 * @param {Array} matches [{ match },...]
 * @param {Array} deliveries [{ delivery },...]
 * @returns {Object} extraRunsConcededObj { team: runs conceded }
 * @exports
 */
function extraRunsConcededPerTeamIn2016(matches, deliveries) {
  const filteredDeliveryData = getDataForYear('2016', matches, deliveries);
  const extraRunsConcededObj = filteredDeliveryData.reduce(
    (extraRunsConcededObj, delivery) => {
      extraRunsConcededObj[delivery.bowling_team] =
        (extraRunsConcededObj[delivery.bowling_team] || 0) +
        +delivery.extra_runs;
      return extraRunsConcededObj;
    },
    {}
  );
  return extraRunsConcededObj;
}

/**
 * Calculates economy of bowlers and writes it over bowler value in arguement object
 *
 * @param {Object} bowlerDataObj { bowler: {total_runs: value, total_balls: value}}
 * @returns {Array} [[ bowler, bowler_economy ]...]
 */
function calculateEconomy(bowlerDataObj) {
  return (
    Object.entries(bowlerDataObj)
      //threshold of 29 balls was decided to differentiate between mainstream bowlers and batsmen
      .filter(bowler => +bowler[1].total_balls > 29)
      //economy calculation
      .map(bowler => {
        bowler[1] = +(
          (bowler[1].total_runs / bowler[1].total_balls) *
          6
        ).toFixed(2);
        return bowler;
      })
  );
}

/**
 * Calculates economy of all bowlers of IPL season 2015 and returns top 10 from the list.
 *
 * @param {Array} matches [{ match },...]
 * @param {Array} deliveries [{ delivery },...]
 * @return {Object} top10EconomicalBowlerObject { bowler: bowler's economy}
 * @exports
 */
function top10EconomicalBowlerIn2015(matches, deliveries) {
  const filteredDeliveryData = getDataForYear('2015', matches, deliveries);
  const bowlerDataObj = filteredDeliveryData
    //total ball and total runs calculation
    .reduce((bowlerDataObj, delivery) => {
      if (!bowlerDataObj.hasOwnProperty(delivery.bowler)) {
        bowlerDataObj[delivery.bowler] = {};
      }
      bowlerDataObj[delivery.bowler].total_runs =
        (bowlerDataObj[delivery.bowler].total_runs || 0) + +delivery.total_runs;
      //to avoid overcounting of balls, constraint of < 7 is placed
      if (delivery.ball < 7) {
        bowlerDataObj[delivery.bowler].total_balls =
          (bowlerDataObj[delivery.bowler].total_balls || 0) + 1;
      }
      return bowlerDataObj;
    }, {});

  //sort it based on index 1(values--economy)
  const sortedEconomicalBowlersArray = calculateEconomy(bowlerDataObj).sort(
    (a, b) => a[1] - b[1]
  );
  //create resultant object
  let top10EconomicalBowlerObject = sortedEconomicalBowlersArray
    //top 10 sliced out
    .slice(0, 10)
    .reduce((topEconomicalObject, bowler) => {
      topEconomicalObject[bowler[0]] = bowler[1];
      return topEconomicalObject;
    }, {});
  return top10EconomicalBowlerObject;
}
module.exports = {
  matchesPlayedPerYear,
  matchesWonPerTeamPerYear,
  extraRunsConcededPerTeamIn2016,
  top10EconomicalBowlerIn2015
};
