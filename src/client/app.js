/**
 * Generates chart based on the arguement object.
 *
 * @param {Object} O-Object with  container, titleText, yAxisText, xAxisText,
 * categoriesValue, stackingValue,seriesValue as properties and their
 * corresponding value.
 */
function plotChart({
  container,
  titleText,
  yAxisText,
  xAxisText,
  categoriesValue,
  stackingValue,
  seriesValue
}) {
  Highcharts.chart(container, {
    chart: {
      type: 'column'
    },
    title: {
      text: titleText
    },

    subtitle: {
      text: 'Source: Kaggle'
    },
    accessibility: {
      announceNewData: {
        enabled: true
      }
    },
    yAxis: {
      title: {
        text: yAxisText
      }
    },
    xAxis: {
      title: {
        text: xAxisText
      },
      categories: categoriesValue
    },
    legend: {
      layout: 'vertical',
      align: 'right',
      verticalAlign: 'middle'
    },

    plotOptions: {
      column: {
        stacking: stackingValue,
        dataLabels: {
          enabled: true
        }
      }
    },

    series: [...seriesValue]
  });
}
/**
 * Fetches the file from a local resource with the filename as given in the arguement
 *
 * @param {string} fileName- Name of file that you want to fetch
 */
function fetchFile(fileName) {
  return fetch(`../output/${fileName}.json`).then(function(response) {
    return response.json();
  });
}
/**
 * Initalizes the whole JS file with necessary arguements.
 */
function main() {
  fetchFile('matchesPlayedPerYear').then(data => {
    plotChart({
      container: 'matchesPlayed',
      titleText: 'Matches Played Per Year',
      yAxisText: 'Matches',
      xAxisText: 'IPL Season',
      stackingValue: null,
      categoriesValue: Object.keys(data),
      seriesValue: [
        {
          name: '',
          data: Object.values(data)
        }
      ]
    });
  });
  fetchFile('matchesWonPerTeamPerYear').then(data => {
    plotChart({
      container: 'matchesWon',
      titleText: 'Matches Won Per Team Per Year',
      yAxisText: 'Teams',
      xAxisText: 'Season',
      categoriesValue: Object.keys(data),
      stackingValue: 'normal',
      seriesValue: stackedColumnDataFormatter(data)
    });
  });
  fetchFile('extraRunsConcededPerTeamIn2016').then(data => {
    plotChart({
      container: 'runsConceded',
      titleText: 'Extra Runs Conceded Per Team (2016)',
      yAxisText: 'Runs Conceded',
      xAxisText: 'Team',
      categoriesValue: Object.keys(data),
      stackingValue: null,
      seriesValue: [
        {
          name: '',
          data: Object.values(data)
        }
      ]
    });
  });
  fetchFile('top10EconomicalBowlerIn2015').then(data => {
    plotChart({
      container: 'topEconomical',
      titleText: 'Top 10 Economical Bowlers (2015)',
      yAxisText: 'Economy',
      xAxisText: 'Player',
      categoriesValue: Object.keys(data),
      stackingValue: null,
      seriesValue: [
        {
          name: '',
          data: Object.values(data)
        }
      ]
    });
  });
}
/**
 * Formats the response object into required array of objects
 * suitable for stacked column chart.
 *
 * @param {Object} response-Response Object fetched from any resource
 * @returns {Array} - Array of formatted data objects
 */
function stackedColumnDataFormatter(response) {
  // create empty teamName array
  let teamNameArray = [];
  //   push all team names from all season objects
  Object.values(response).forEach(season => {
    teamNameArray.push(...Object.keys(season));
  });
  //   remove redundancy by using Set
  teamNameArray = [...new Set(teamNameArray)];
  // format data as "team_name":[matches_won_in_season_x,...]
  const resultObj = Object.values(response).reduce((resultObj, season) => {
    teamNameArray.forEach(team => {
      if (!resultObj.hasOwnProperty(team)) {
        resultObj[team] = [];
      }
      if (season.hasOwnProperty(team)) {
        resultObj[team].push(season[team]);
      } else {
        resultObj[team].push(0);
      }
    });
    return resultObj;
  }, {});
  //   return an array of objects where each object has towo properties where one
  //   is team name and the other is data with values 'team name' and
  //   matche won array, respectively
  return Object.entries(resultObj).reduce((resultArray, team) => {
    resultArray.push({ name: team[0], data: team[1] });
    return resultArray;
  }, []);
}
